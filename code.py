import pyodbc
import config as conf
from shutil import copy2
import os
from time import gmtime, strftime


def logScript(start,end):
    logString = "Script Started At: "+start+" | Script Ended At: "+end+" | Success\n"
    with open(conf.LOG_DIR, "a+") as logFile:
        logFile.write(logString)


def getConnection():
    connection_stirng='DRIVER={'+conf.DRIVER+'}'+';'+'SERVER='+conf.SERVER+';'+'DATABASE='+conf.DB+';'+'UID='+conf.UID+';'+'PWD='+conf.PWD
    connection = pyodbc.connect(connection_stirng, autocommit=True)
    return connection

def closeConnection(connection):
    connection.close()

def makeFullBackup(connection):
    cursor = connection.cursor()
    query = "BACKUP DATABASE ["+conf.DB+"] TO DISK = N'"+conf.WINDOWS_PARTITION+conf.FULL_DB+"' WITH CHECKSUM;"
    cursor.execute(query)
    while cursor.nextset():
        pass
    closeConnection(connection)
    copyFullBackup()

def makeDifferentialBackup(connection):
    cursor = connection.cursor()
    query = "BACKUP DATABASE ["+conf.DB+"] TO DISK = N'"+conf.WINDOWS_PARTITION+conf.DIFF_DB+"' WITH DIFFERENTIAL;"
    cursor.execute(query)
    while cursor.nextset():
        pass
    closeConnection(connection)
    copyDifferentialBackup()

def deleteDifferentialBackup():
    src = conf.MOUNT_PATH+conf.DIFF_DB
    os.remove(src)
    return

def deleteFullBackup():
    src = conf.MOUNT_PATH+conf.FULL_DB
    os.remove(src)
    return

def copyDifferentialBackup():
    src = conf.MOUNT_PATH+conf.DIFF_DB
    dst = conf.STORE_PATH+conf.DIFF_DB
    copy2(src,dst)
    deleteDifferentialBackup()
    return

def copyFullBackup():
    src = conf.MOUNT_PATH+conf.FULL_DB
    dst = conf.STORE_PATH+conf.FULL_DB
    copy2(src,dst)
    deleteFullBackup()
    return

start=strftime("%Y-%m-%d-%H-%M-%S", gmtime())

if conf.CURRENT_DAY in conf.DIFF_BACKUP:
    makeDifferentialBackup(getConnection())
elif conf.CURRENT_DAY == conf.FULL_BACKUP:
    makeDifferentialBackup(getConnection())
    makeFullBackup(getConnection())

end=strftime("%Y-%m-%d-%H-%M-%S", gmtime())
logScript(start,end)