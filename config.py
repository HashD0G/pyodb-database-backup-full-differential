# config file for mssql connection
#specify driver
DRIVER='ODBC Driver 17 for SQL Server'
#specify server address
SERVER='XXX.XXX.XXX.XXX,PORT'
#specify database name
DB='database_name'
#specify UID
UID='db_username'
#specify PWD
PWD='db_password'
#specify the path for the shared linux-windows path
MOUNT_PATH='path where windows partition is mounted'
#specify the backup_path  to copy from the shared folder to backup folder
STORE_PATH='path where you want to store database backup'

# Full Backup Day (4 - Friday) | Monday -> Sunday == 0 -> 6
FULL_BACKUP = 4

# Differential Backup Day
DIFF_BACKUP = [0,1,2,3,5,6]

DAYS = {
    0 : "Monday",
    1 : "Tuesday",
    2 : "Wednesday",
    3 : "Thursday",
    4 : "Friday",
    5 : "Saturday",
    6 : "Sunday",
}

# File name of full backup
FULL_BACKUP_NAME = "Full-Backup"
# Partial File name of differential backup
DIFF_BACKUP_NAME = "Diff-Backup-"
# Backup file extension
EXT = ".bak"
# Path of Windows Partition Where you want to temporarily store database before moving
WINDOWS_PARTITION="Y://"
# Path for loggin script execution
LOG_DIR = "/var/log/pyodbc_backup.txt"

import datetime as dt
# Monday is 0 | Sunday is 6
CURRENT_DAY = dt.datetime.today().weekday()
FULL_DB = FULL_BACKUP_NAME+EXT
DIFF_DB = DIFF_BACKUP_NAME+DAYS[CURRENT_DAY]+EXT