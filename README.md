# PYODB DATABASE BACKUP (FULL / DIFFERENTIAL)

This program was created in need to backup Microsoft SQL Database to a linux machine. Since many tools are paid, we created this script to fulfill our demand.

This program connects to a database server (Windows Server Machine) and creates a backup (Full / Diff) to a local partition (shared) which is mounted on a linux machine.
Once the file is created it is moved to a local directory on the linux machine. This script can be used on local server machine as well. 

The script is run on cron tab which creates daily differential backup and one full backup on the designated day of the week. everytime the script executes it creates a simple log.

Feel free to use this, distribute this or improve this. 